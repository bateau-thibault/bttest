import { Component, OnInit, Input } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { Product, ProductsService } from 'src/app/core/services/products.service';
import { FormsModule } from '@angular/forms'; // Add this line
// ...
@Component({
  selector: 'app-details-produits',
  templateUrl: './details-produits.component.html',
  styleUrls: ['./details-produits.component.css']
})
export class DetailsProduitsComponent implements OnInit {
  listeProduits: Product[] = [];
  poissons: Product[] = [];
  fruitsDeMer: Product[] = [];
  crustaces: Product[] = []; 

  produit = {
    stock: ''
  }

  constructor(private productsService: ProductsService) {}

  async ngOnInit() {
    await this.getProducts();
  }

  async getProducts() {
    try {
      const res: Product[] | undefined = await this.productsService.getProductsFromJson().toPromise();
      if (res !== undefined) {
        this.listeProduits = res;
      } else {
        console.error('Les données JSON sont undefined.');
      }
    } catch (err) {
      console.error('Erreur lors du chargement des données JSON :', err);
    }
  }

  getProductsByCategory(categoryIndex: number): Product[] {
    switch (categoryIndex) {
      case 0:
        return this.listeProduits.filter((produit) => produit.category === 0);
      case 1:
        return this.listeProduits.filter((produit) => produit.category === 1);
      case 2:
        return this.listeProduits.filter((produit) => produit.category === 2);
      default:
        return [];
    }
  }

  modifierStock(produit: Product) {
    if (produit.modifiedStock !== undefined) {
      if(produit.modifiedStock+produit.quantity_stock < 0)
      {
        produit.quantity_stock = 0;
      }else{
        produit.quantity_stock = produit.modifiedStock+produit.quantity_stock;
      }
      this.productsService.updateProductStock(produit).subscribe(
        (updatedProduct) => {
          // Gérez la réponse si nécessaire
          console.log('Stock mis à jour avec succès :', updatedProduct);
        },
        (error) => {
          console.error('Erreur lors de la mise à jour du stock :', error);
        }
      );
    }
  }
  //ajouter le prix d'achat

  modifierPrixAchat(produit: Product) {
    if (produit.achat_prix !== undefined) {    
      //perte
      if(produit.achat_prix == 0)
      {
        //rentrer dans la catégorie perte
      }
      else 
      {
        //inserer dans le SQL le prix d'achat
      }
      this.productsService.updateProductPriceAndStock(produit).subscribe(
        (updatedProduct) => {
          // Gérez la réponse si nécessaire
          console.log('Stock mis à jour avec succès :', updatedProduct);
        },
        (error) => {
          console.error('Erreur lors de la mise à jour du stock :', error);
        }
      );
    }
  }
  IsNan(number:Number){
    return Number.isNaN(number);
  }
  modifierPourcentage(produit: Product) {
    if (produit.modifiePourcentage !== undefined) {            
      produit.discount = produit.modifiePourcentage;
      console.log(produit.discount);

      this.productsService.updatedPourcentageProduct(produit).subscribe(
        (updatedPourcentage) => {
          // Gérez la réponse si nécessaire
          console.log('Pourcentage mis à jour avec succès :', updatedPourcentage);
        },
        (error) => {
          console.log("produit ", produit);
          console.error('Erreur lors de la mise à jour du porcentage :', error);
        }
      );
    }
  }
  
  modifierStockList(produits: Product[]) {
    const observables: Observable<Product>[] = [];
  
    produits.forEach(produit => {
      if (produit.modifiedStock !== undefined) {


        if (produit.modifiedStock + produit.quantity_stock < 0) 
        {
          produit.quantity_stock = 0;
        } else {
          produit.quantity_stock = produit.modifiedStock + produit.quantity_stock;
        }
        const stockUpdateObservable = this.productsService.updateProductStockList(produit);
  
        observables.push(stockUpdateObservable);
      }
    });
  
    // forkJoin pour envoyer toutes les mises à jour de stock en même temps
    if (observables.length > 0) {
      forkJoin(observables).subscribe(
        (updatedProducts) => {
          // Géestion erreur
          console.log('Stocks mis à jour avec succès :', updatedProducts);
        },
        (error) => {
          console.error('Erreur lors de la mise à jour des stocks :', error);
        }
      );
    }
  }

}
