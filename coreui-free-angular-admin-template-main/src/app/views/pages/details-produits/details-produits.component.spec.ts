import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsProduitsComponent } from './details-produits.component';
import { ButtonModule, CardModule, FormModule, GridModule } from '@coreui/angular';
import { IconModule } from '@coreui/icons-angular';
import { IconSetService } from '@coreui/icons-angular';
import { iconSubset } from '../../../icons/icon-subset';


describe('DetailsProduitsComponent', () => {
  let component: DetailsProduitsComponent;
  let fixture: ComponentFixture<DetailsProduitsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsProduitsComponent],
      imports: [CardModule, FormModule, GridModule, ButtonModule, IconModule],
      providers: [IconSetService]
    })
    .compileComponents();
  });
  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsProduitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
