import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private products: Product[] = [];
  
  constructor(private http: HttpClient) { }
  private productsUrl = '../../assets/Products.json';

  getProductsFromJson(): Observable<Product[]> {
    return this.http.get<Product[]>(this.productsUrl);
  }
  updateProductStock(product: Product): Observable<Product> {
    return this.http.put<Product>(this.productsUrl, product);
  }
  updatedPourcentageProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(this.productsUrl, product);
  }
  updateProductStockList(produit: Product): Observable<Product> {
    const updateData = {
      id: produit.id,  
      quantity_stock: produit.quantity_stock
    };
    return this.http.put<Product>(`${this.productsUrl}/${produit.id}`, updateData);
  }
  updateProductPriceAndStock(produit:Product): Observable<Product>{
    const updateData = {
      id: produit.id,  
      achat_prix: produit.achat_prix,
      quantity_stock :produit.quantity_stock
    };
    return this.http.put<Product>(`${this.productsUrl}/${produit.id}`, updateData);
  }
}

export interface Product {
    id: number;
    unit: string;
    category: number;
    name: string;
    discount: number;
    comments: string;
    owner: string;
    price: number;
    price_on_sale: number;
    sale: boolean;
    availability: boolean;
    quantity_stock: number;
    quantity_sold: number;
    modifiedStock:number;
    modifiePourcentage:number;
    achat_prix: number;
    
}