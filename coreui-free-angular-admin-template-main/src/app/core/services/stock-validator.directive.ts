import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appStockValidator]'
})
export class StockValidatorDirective {
  private regex: RegExp = new RegExp(/[.,]/);

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  @HostListener('input', ['$event']) onInput(event: Event): void {
    const inputElement = event.target as HTMLInputElement;
    const stockValue = inputElement.value;

    if (this.regex.test(stockValue)) {
      // La valeur du stock contient un point ou une virgule, définissez le fond en rouge
      this.renderer.setStyle(inputElement, 'background-color', 'red');
    } else {
      // La valeur du stock est valide, réinitialisez le fond à sa couleur d'origine
      this.renderer.removeStyle(inputElement, 'background-color');
    }
  }
}
